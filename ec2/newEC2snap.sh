#!/bin/bash

ec2instance=$1
SNAP_ID=`aws ec2 create-snapshot --volume-id $(aws ec2 describe-instances --filters "Name=tag:Name,Values=$1" --query  'Reservations[].Instances[].BlockDeviceMappings[].Ebs[].[VolumeId]' --output text
) --query 'SnapshotId' --output text`
aws ec2 create-tags --resources $SNAP_ID --tags Key=Name,Value=$ec2instance-snap
