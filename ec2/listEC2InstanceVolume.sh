#!/bin/bash


# One Volume
aws ec2 describe-instances --filters "Name=tag:Name,Values=$1" --query  'Reservations[].Instances[].BlockDeviceMappings[].Ebs[].[VolumeId]' --output text

# all volumes
#aws ec2 describe-instances --filters "Name=tag:Name,Values=*" --query  'Reservations[].Instances[].BlockDeviceMappings[].Ebs[].[VolumeId]' --output text

#aws ec2 describe-tags --query "Tags[*].{Name:Value,ResourceId:ResourceId}" --filters "Name=key,Values=Name"  --filters "Name=resource-type,Values=volume" --output json
