# Scripts for EC2

## listEC2InstanceVolume.sh
./listEC2InstanceVolume.sh agent01 will show you the volume ID for the volume assing to your EC2 Instance tag with agent01


## Snaping EC2 Instances
./newEC2snap.sh instancetag 

This will do a query for the instance tag and will get the volume ID. Then use that Volume ID to take a snapshot. The Snapshot will be named instancetag-snap
