#!/bin/bash

# Author: Carlos Vargas

echo "Starting Mesosphere Bootstrap01"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=bootstrap01" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Starting Mesosphere Master01"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=master01" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Starting Mesosphere Agent01"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=agent01" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Starting Mesosphere Agent02"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=agent02" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Waiting 1 min to bypass AWS Boot Limitation"
sleep 60
echo "Starting Mesosphere Agent03"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=agent03" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Starting Mesosphere Agent04"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=agent04" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Starting Mesosphere Agent05"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=agent05" --query 'Reservations[].Instances[].[InstanceId]' --output text)
echo "Starting Mesosphere Public01"
aws ec2 stop-instances --instance-ids $(aws ec2 describe-instances --filters "Name=tag:Name,Values=public01" --query 'Reservations[].Instances[].[InstanceId]' --output text)

