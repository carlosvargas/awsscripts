#!/bin/bash
#
# author: Carlos Vargas
# version: 1.0
# 
# Script to Build EC2 Environment as for Mesosphere Cluster

clear
# Header
echo "********************* Build Infrastrucutre Resources in AWS for Mesosphere Cluster **********************"
echo "*******************************************Author: Carlos Vargas*****************************************"
echo ""

# Check if script was called with the appropiate arguments
if (($# != 4 )); then
   echo "The script needs 4 parameters. Example: ./buildMesosphereInAWS.sh region masters agents public"
   echo "If you want to deploy a cluster of 3 masters, 5 agents and 2 public agents then use this example"
   echo "./buildMesosphereInAWS.sh "us-east-1" 3 5 2"
else

#Variables for Script
awsregion=$1
vpctag="mesospherevpc"
pubsubnet="mesosphere-pub-vnet"
privsubnet="msosphere-priv-vnet"
internetgateway="mesosphere-igw"
mainrt="mesospere-main-route"
customrt="mesosphere-custom-route"
natami="ami-311a1a5b"
centosami="ami-6d1c2007"
defaultgrp="mesosphere-def-grp"
sshgroupname="SSHAccess"
masterssecuritygroup="MastersAccess"
publicsecuritygroup="PublicAgentAccess"
mesosec2instancesize="t2.xlarge"
natec2instancesize="t2.micro"
bootstrapname="bootstrap01"
natinstancename="natinstance"
masters=$2
mastersize="t2.micro"
agents=$3
agentsize="t2.micro"
publicagent=$4
publicsize="t2.micro"

# Create a VPC
echo "Creating VPC"
vpcid=$(aws ec2 create-vpc --cidr-block 10.0.0.0/22 --output text| awk '{print $7}')
sleep 15

#Create VPC Tag Tags
echo Renaming VPC $vpcid to $vpctag
aws ec2 create-tags --resources $vpcid --tags Key=Name,Value=$vpctag
sleep 15

#Create Public Subnet
echo "Creating Public Subnet"
publicsubnetaz=$awsregion"a"
publicsubnet=$(aws ec2 create-subnet --vpc-id $vpcid --cidr-block 10.0.0.0/24 --availability-zone $publicsubnetaz  --output text| awk '{print $9}')
aws ec2 create-tags --resources $publicsubnet --tags Key=Name,Value=$pubsubnet
sleep 15

#Create Private Subnet
echo "Creating Private Subnet"
privatesubnetaz=$awsregion"d"
privatesubnet=$(aws ec2 create-subnet --vpc-id $vpcid --cidr-block 10.0.1.0/24 --availability-zone $privatesubnetaz --output text| awk '{print $9}')
aws ec2 create-tags --resources $privatesubnet --tags Key=Name,Value=$privsubnet
sleep 15

#Create Internet Gateway
echo "Creating Internet Gateway"
igw=$(aws ec2 create-internet-gateway --output text|awk '{print $2}')
aws ec2 create-tags --resources $igw --tags Key=Name,Value=$internetgateway
sleep 15

#Attach Internet Gateway to VPC
echo "Attaching Internet Gateway to VPC" $vpctag
aws ec2 attach-internet-gateway --vpc-id $vpcid --internet-gateway-id $igw
sleep 15

#Get Main Route Table
echo "Getting Main Routing Table for VPC" $vpctag
mainroutetable=$(aws ec2 describe-route-tables --filters "Name=vpc-id,Values=$vpcid" --output text --query 'RouteTables[].[RouteTableId]')
aws ec2 create-tags --resources $mainroutetable --tags Key=Name,Value=$mainrt
sleep 15

#Create Custom Route Table
echo "Creating Custom Routing Table for VPC" $vpctag
customroutetable=$(aws ec2 create-route-table --vpc-id $vpcid --output text|awk 'NR==1{print $2}')
aws ec2 create-tags --resources $customroutetable --tags Key=Name,Value=$customrt
sleep 15

# Attach Internet Gateway to Custom Routing Table
echo Attaching Internet Gateway to Custom Routing Table $customroutetable
attachgwlog=$(aws ec2 create-route --route-table-id $customroutetable --destination-cidr-block 0.0.0.0/0 --gateway-id $igw)
sleep 15

# Associate Public Subnet to Custom Routing Table
echo Associate Public Subnet with Custom Routing Table
publicsubattachlog=$(aws ec2 associate-route-table --subnet-id $publicsubnet --route-table-id $customroutetable)
sleep 15

# Automatically Assign Public IP address in Public Subnet
echo Change Public IP Address Asssignment in Public Subnet
aws ec2 modify-subnet-attribute --subnet-id $publicsubnet --map-public-ip-on-launch
sleep 15

# Get Default Security Group in VPC
echo Configure VPC default Security Group
defaultvpcgroup=$(aws ec2 describe-security-groups --filters "Name=vpc-id,Values=$vpcid" --query 'SecurityGroups[].[GroupId]' --output text)
aws ec2 create-tags --resources $defaultvpcgroup --tags Key=Name,Value=$defaultgrp
aws ec2 authorize-security-group-ingress --group-id $defaultvpcgroup --protocol all --cidr 0.0.0.0/0
sleep 15

# Get KeyPair Name
sshkeypair=$(aws ec2 describe-key-pairs --query 'KeyPairs[].[KeyName]' --output text)
sleep 15

# Create new security group for ssh
echo "Configuring SSH and Web Access for Bootstrap"
sshsecgroup=$(aws ec2 create-security-group --group-name SSHAccess --description "Security group for SSH access" --vpc-id $vpcid --output text |awk '{print $1}')
aws ec2 create-tags --resources $sshsecgroup --tags Key=Name,Value=$sshgroupname
sleep 10

# Add Security Group Ingress
aws ec2 authorize-security-group-ingress --group-id $sshsecgroup --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $sshsecgroup --protocol tcp --port 80 --cidr 10.0.0.0/16
sleep 10

# Download UpdateInstance.sh - update instances as they boot up
echo "Downloading updateinstance.sh"
downloadlog=$(curl https://gitlab.com/carlosvargas/awsscripts/raw/master/ec2/updateinstance.sh > /tmp/updateinstance.sh 2> /dev/null)

# Deploy your jump box for access to aws
echo "Deploying Bootstrap01"
bootstrap01=$(aws ec2 run-instances --image-id $centosami --count 1 --instance-type t2.micro --key-name $sshkeypair --security-group-ids $SSHAccess --private-ip-address 10.0.0.254 --subnet-id $publicsubnet --block-device-mappings "DeviceName"="/dev/sda1","Ebs"={"VolumeSize"="120"} --user-data "$(cat /tmp/updateinstance.sh)" --query 'Instances[].[InstanceId]' --output text)
sleep 5
aws ec2 create-tags --resources $bootstrap01 --tags Key=Name,Value=$bootstrapname
sleep 5

# Deploy NAT
echo "Deploying NAT Instance"
natec2instances=$(aws ec2 run-instances --image-id $natami --count 1 --instance-type $natec2instancesize --key-name $sshkeypair --security-group-ids $defaultvpcgroup --subnet-id $publicsubnet --query 'Instances[].[InstanceId]' --output text)
sleep 5
aws ec2 create-tags --resources $natec2instances --tags Key=Name,Value=$natinstancename
sleep 15

# Creating Load Balancers
# Create ELB for masters
## Create Masters ELB Security Group
echo "Creating Masters ELB for Web Access"
masterssecgroupid=$(aws ec2 create-security-group --group-name $masterssecuritygroup --description "Security group for Mesosphere Masters access" --vpc-id $vpcid --output text |awk '{print $1}')
sleep 5
aws ec2 create-tags --resources $masterssecgroupid --tags Key=Name,Value=$masterssecuritygroup
sleep 10
### Add Security Group Ingress Rules
aws ec2 authorize-security-group-ingress --group-id $masterssecgroupid --protocol tcp --port 80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $masterssecgroupid --protocol tcp --port 443 --cidr 0.0.0.0/0
sleep 10

### Create Masters ELB
masterlbname="mesospheremasters"
masterslbdnsname=$(aws elb create-load-balancer --load-balancer-name $masterlbname --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=80" "Protocol=TCP,LoadBalancerPort=443,InstanceProtocol=TCP,InstancePort=443" "Protocol=HTTP,LoadBalancerPort=8181,InstanceProtocol=HTTP,InstancePort=8181" --subnets $privatesubnet --security-groups $masterssecgroupid --output text)
echo "Masters Load Balancer:" $masterslbdnsname
sleep 10

## Create ELB for Public Agents
echo "Configuring Public Agents Web Access"
publicsecgroupid=$(aws ec2 create-security-group --group-name $publicsecuritygroup --description "Security group for Mesosphere Public Agent access" --vpc-id $vpcid --output text |awk '{print $1}')
sleep 5
aws ec2 create-tags --resources $publicsecgroupid --tags Key=Name,Value=$publicsecuritygroup
sleep 10
### Add Security Group Ingress Rules
aws ec2 authorize-security-group-ingress --group-id $publicsecgroupid --protocol tcp --port 80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $publicsecgroupid --protocol tcp --port 443 --cidr 0.0.0.0/0
sleep 10

### Create Public ELB
publiclbname="publicmesos"
publiclbdnsname=$(aws elb create-load-balancer --load-balancer-name $publiclbname --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=80" "Protocol=TCP,LoadBalancerPort=443,InstanceProtocol=TCP,InstancePort=443" "Protocol=HTTP,LoadBalancerPort=9090,InstanceProtocol=HTTP,InstancePort=9090" --subnets $privatesubnet --security-groups $publicsecgroupid --output text)
echo "Public Load Balancer:" $publiclbdnsname
sleep 10

# Deploy masters
echo "Deploying Master Nodes"
for ((i=1;$i<=$masters;i++)); do
    echo Deploying Mesosphere master0$i
    instancename=master0$i
    instancenum=$(aws ec2 run-instances --image-id $centosami --count 1 --instance-type $mastersize --private-ip-address 10.0.1.1$i --key-name $sshkeypair --security-group-ids $defaultvpcgroup --subnet-id $privatesubnet --block-device-mappings "DeviceName"="/dev/sda1","Ebs"={"VolumeSize"="120"} --user-data "$(cat /tmp/updateinstance.sh)" --query 'Instances[].[InstanceId]' --output text)
    aws ec2 create-tags --resources $instancenum --tags Key=Name,Value=$instancename
    echo Attaching Mesosphere master0$i to $masterlbname
    masterlbjoin=$(aws elb register-instances-with-load-balancer --load-balancer-name $masterlbname --instances $instancenum)
    sleep 5
done

# Deploy agents
echo "Deploying Agents Nodes"
for ((i=1;$i<=$agents;i++)); do
    echo Deploying Mesosphere agent0$i
    instancename=agent0$i
    instancenum=$(aws ec2 run-instances --image-id $centosami --count 1 --instance-type $agentsize --private-ip-address 10.0.1.2$i --key-name $sshkeypair --security-group-ids $defaultvpcgroup --subnet-id $privatesubnet --block-device-mappings "DeviceName"="/dev/sda1","Ebs"={"VolumeSize"="120"} --user-data "$(cat /tmp/updateinstance.sh)" --query 'Instances[].[InstanceId]' --output text)
    aws ec2 create-tags --resources $instancenum --tags Key=Name,Value=$instancename
    sleep 5
done

# Deploy public agents
echo "Deploying Public Agents"
for ((i=1;$i<=$publicagent;i++)); do
    echo Deploying Mesosphere public0$i
    instancename=public0$i
    instancenum=$(aws ec2 run-instances --image-id $centosami --count 1 --instance-type $publicsize --private-ip-address 10.0.1.3$i --key-name $sshkeypair --security-group-ids $defaultvpcgroup --subnet-id $privatesubnet --block-device-mappings "DeviceName"="/dev/sda1","Ebs"={"VolumeSize"="120"}  --user-data "$(cat /tmp/updateinstance.sh)" --query 'Instances[].[InstanceId]' --output text)
    aws ec2 create-tags --resources $instancenum --tags Key=Name,Value=$instancename
    echo Attaching Mesosphere public0$i to $publiclbname
    publiclbjoin=$(aws elb register-instances-with-load-balancer --load-balancer-name $publiclbname --instances $instancenum)
    sleep 5
done

# Add NAT Instance as a Route for the Private Subnet
# Get NAT Network Interface ID
echo "Configuring Internet Access via NAT Instance"
natinstanceinterface=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=natinstance" --query 'Reservations[].Instances[].NetworkInterfaces[].[NetworkInterfaceId]' --output text)
#aws ec2 create-route --route-table-id $mainroutetable --destination-cidr-block 0.0.0.0/0 --instance-id $natec2instances --network-interface-id $natinstanceinterface
addnatinstancelog=$(aws ec2 create-route --route-table-id $mainroutetable --destination-cidr-block 0.0.0.0/0 --network-interface-id $natinstanceinterface)

# Change Source/Dest Checking
changesourcedest=$(aws ec2 modify-instance-attribute --instance-id $natec2instances --no-source-dest-check)

# Gather bootstrap01 public IP address
sleep 5
bootstrappubip=$(aws ec2 describe-instances --filters "Name=tag:Name,Values=bootstrap01" --query 'Reservations[].Instances[].NetworkInterfaces[].Association[].[PublicIp]' --output text)

# Outputs
echo ""
echo "Your SSH Connection IP address is:" $bootstrappubip
echo "Your Masters Nodes ELB is:" $masterslbdnsname
echo "Your Public Nodes ELB is:" $publiclbdnsname

# End of Scrpt
fi
